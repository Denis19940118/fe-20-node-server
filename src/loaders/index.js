import expresLoader from './expressLoader';

export default async ({ config }) => {
	const app = expresLoader({ config });

	return app;
};
