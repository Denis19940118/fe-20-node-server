import appLoader from './loaders';

(async function () {
	const port = process.env.PORT || 3000;
	const app = await appLoader({ config: {} });
	app.listen(port, err => {
		if (err) {
			console.log('Error has just happened --> ', err);
			process.exit(1);
			return;
		}
		console.log(`
        ################################################
        🛡️ Server listening on port: ${port}
        ################################################`);
	});
})();

// import express from 'express';
//
// const app = express();
//
//
// app.get('/', (request, response) => {
// 	response.send('Hello from Express!')
// });
//
// app.get('/about', (request, response) => {
// 	response.send('About page!');
// });
//

// import appLoader from './loaders/index';

// const express = require('express');
// const app = express();
// const port = process.env.PORT || 3000;

// app.get('/', (request, response) => {
// 	response.send('Hello from Express!');
// });

// app.get('/about', (request, response) => {
// 	response.send('about page!');
// });

// app.listen(port, err => {
// 	if (err) {
// 		return console.log('something bad happened', err);
// 	}
// 	console.log(`server is listening on ${port}`);
// });

// const http = require('http');
// const port = process.env.PORT || 3000;

// const requestHandler = (request, response) => {
// 	console.log(request.url, request.method);
// 	switch (request.method) {
// 		case 'POST':
// 			if (request.url === '/') {
// 				return response.end('Hello Node.js Server for POST request FOR /!');
// 			} else if (request.url === '/about') {
// 				return response.end('You have just visited the about page!');
// 			}
// 			return response.end('Hello Node.js Server for POST request!');
// 	}
// 	response.end('Hello Node.js Server!');
// };
// const server = http.createServer(requestHandler);
// server.listen(port, err => {
// 	if (err) {
// 		return console.log('something bad happened', err);
// 	}
// 	console.log(`server is listening on ${port}`);
// });

// // for (let i = 0 ; i < 10000; i++){
// //     console.log(`hello ${i}`)
// // }

// // //  * @param {*} a
// // //  * @param {*} b
// // //  * @param  {...any} rest
// // /**
// //  *@desc Sum for a few numbers
// //  *@return {NUmber}
// //  **/
// // function sum(a, b, ...rest) {
// // 	const arr = [a, b, ...rest];

// // 	return rest.reduce((gen, val) => {
// // 		return gen + val;
// // 	}, a + b);
// // }// export.sum = sum;
// const Math = require('.lib/math.js');
// const lib = require('.lib');

// console.log('math', math);
// console.log(`Sum -> ${lib.sum(1, 2, 3, 4, 5, 6, 7)}`);
// console.log(`Sum2 -> ${lib.sum(0, -2, 3)}`);
